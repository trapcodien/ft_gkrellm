#include <iostream>
#include "ExampleDisplay.hpp"
#include "ExampleModule.hpp"

int		main(void)
{
	ExampleDisplay 	displayer;

	ExampleModule 	module1;
	ExampleModule 	module2("TITRE", "TEXTE");
	ExampleModule 	module3("OS", "Max Os X");

	displayer[0] = &module1;
	displayer[1] = &module2;
	displayer[2] = &module3;

	displayer.startMonitoring(100);
	return 0;
}

