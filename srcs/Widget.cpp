#include "Widget.hpp"

	// Public Constructor
Widget::Widget(std::string const & name)
: _name(name) {  }

	// Coplien
Widget::Widget()
: _name("Default Blank Widget") {  }

Widget::~Widget()
{
	
}

Widget::Widget(Widget const & ref)
{
	*this = ref;
}

Widget &				Widget::operator=(Widget const & rhs)
{
	this->_name = rhs.getName();
	return (*this);
}

	// Getters
std::string const & 	Widget::getName(void) const { return this->_name; }

void					Widget::display(IMonitorDisplay const & displayer) const
{
	displayer.displayWidget(*this);
}
