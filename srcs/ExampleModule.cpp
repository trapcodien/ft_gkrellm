#include <sstream>
#include "ExampleModule.hpp"

	// Private Static
std::string				ExampleModule::intToStr(int n)
{
	std::stringstream	ss;
	std::string			str;

	ss << n;
	ss >> str;
	return str;
}

	// Constructor
ExampleModule::ExampleModule(std::string const & title, std::string const & text)
: _title(title), _nbRefresh(0)
{
	WidgetText *	first = new WidgetText("Text", text);
	WidgetText *	second = new WidgetText("NbRefresh", ExampleModule::intToStr(0));

	this->_widgets.push_back(first);
	this->_widgets.push_back(second);
}

	// Coplien
ExampleModule::ExampleModule() : _title("Example Module"), _nbRefresh(0)
{
	WidgetText *	first = new WidgetText("Text", "Hello World, this is the default example module.");
	WidgetText *	second = new WidgetText("NbRefresh", ExampleModule::intToStr(0));

	this->_widgets.push_back(first);
	this->_widgets.push_back(second);
}

ExampleModule::~ExampleModule()
{
	Widgets::iterator it;

	for (it = this->_widgets.begin() ; it != this->_widgets.end() ; it++)
		delete *it;
}

ExampleModule::ExampleModule(ExampleModule const & ref) : IMonitorModule()
{
	*this = ref;
}

ExampleModule &	ExampleModule::operator=(ExampleModule const & rhs)
{
	this->_title = rhs.getTitle();
	this->_widgets = rhs.getWidgets();
	this->_nbRefresh = rhs.getNbRefresh();
	return (*this);
}


Widgets const & 		ExampleModule::getWidgets(void) const { return this->_widgets; }

	// Interface
std::string const &		ExampleModule::getTitle(void) const { return this->_title; }
void					ExampleModule::setTitle(std::string const & title) { this->_title = title; }
void					ExampleModule::exec(void)
{
	WidgetText *		ptr;

	ptr = dynamic_cast<WidgetText *>(this->_widgets[1]);
	if (ptr)
	{
		this->_nbRefresh++;
		ptr->setText(this->intToStr(this->_nbRefresh));
	}
}

	// Getter
int						ExampleModule::getNbRefresh(void) const { return this->_nbRefresh; }

