#include "WidgetText.hpp"

	// Constructors
WidgetText::WidgetText(std::string const & name)
: Widget(name) {  }

WidgetText::WidgetText(std::string const & name, std::string const & text)
: Widget(name), _text(text) {  }

	// Coplien
WidgetText::WidgetText()
: Widget("Default Text Widget") {  }

WidgetText::~WidgetText() {  }

WidgetText::WidgetText(WidgetText const & ref)
: Widget(ref.getName()) { *this = ref; }

WidgetText &	WidgetText::operator=(WidgetText const & rhs)
{
	this->_text = rhs.getText();
	return (*this);
}

	// Getters
std::string const &		WidgetText::getText(void) const { return this->_text; }

	// Setters
void					WidgetText::setText(std::string const & text)
{ this->_text = text; }

void 					WidgetText::display(IMonitorDisplay const & displayer) const
{
	displayer.displayWidgetText(*this);
}
