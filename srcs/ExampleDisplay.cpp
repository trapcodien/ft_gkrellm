#include <unistd.h>
#include <cstdlib>
#include "ExampleDisplay.hpp"

	// Init Function
void				ExampleDisplay::initModules(void)
{
	this->_modules[0] = NULL;
	this->_modules[1] = NULL;
	this->_modules[2] = NULL;
}

	// Coplien
ExampleDisplay::ExampleDisplay()
{
	this->initModules();
}

ExampleDisplay::~ExampleDisplay()
{
	
}

ExampleDisplay::ExampleDisplay(ExampleDisplay const & ref)
: IMonitorDisplay()
{
	*this = ref;
}

ExampleDisplay &	ExampleDisplay::operator=(ExampleDisplay const & rhs)
{
	ExampleDisplay *	ptr;

	ptr = const_cast<ExampleDisplay *>(&rhs);
	this->_modules[0] = (*ptr)[0];
	this->_modules[1] = (*ptr)[1];
	this->_modules[2] = (*ptr)[2];
	return (*this);
}

	// Interface
IMonitorModule *&	ExampleDisplay::operator[](int index)
{
	if (index < 0 || index >= 3)
		throw std::exception();
	return this->_modules[index];
}

void				ExampleDisplay::startMonitoring(int time)
{
	int				size = 3;
	IMonitorModule	*module;

	while (42)
	{
		system("clear");
		for (int i = 0 ; i < size ; i++)
		{
			module = this->_modules[i];
			if (module)
			{
				std::cout << "--- " << module->getTitle() << " ---" << std::endl;
				module->exec();
				for (unsigned int i = 0 ; i < module->getWidgets().size() ; i++)
					module->getWidgets()[i]->display(*this);
				std::cout << std::endl;
			}
		}
		usleep(1000 * time);
	}
}

void				ExampleDisplay::displayWidget(Widget const & w) const
{
	std::cout << w.getName() << std::endl;
}

void				ExampleDisplay::displayWidgetText(WidgetText const & w) const
{
	std::cout << w.getName() << " : " << w.getText() << std::endl;
}
