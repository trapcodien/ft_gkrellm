# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: garm <garm@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/08/26 23:49:31 by garm              #+#    #+#              #
##   Updated: 2015/01/17 18:16:28 by garm             ###   ########.fr       ##
#                                                                              #
# **************************************************************************** #

CC = g++

NAME = ft_gkrellm

SOURCES_DIR = srcs
INCLUDES_DIR = includes

ifeq ($(DEBUG), 1)
	FLAGS = -g -Wall -Wextra
	CC = g++
else
	FLAGS = -Wall -Werror -Wextra
endif

CFLAGS += $(FLAGS) -I $(INCLUDES_DIR)

DEPENDENCIES = \
				$(INCLUDES_DIR)/ExampleDisplay.hpp \
				$(INCLUDES_DIR)/ExampleModule.hpp \
				$(INCLUDES_DIR)/IMonitorModule.hpp \
				$(INCLUDES_DIR)/IMonitorDisplay.hpp \
				$(INCLUDES_DIR)/Widget.hpp \
				$(INCLUDES_DIR)/WidgetText.hpp \
				

SOURCES = \
		  $(SOURCES_DIR)/ExampleDisplay.cpp \
		  $(SOURCES_DIR)/ExampleModule.cpp \
		  $(SOURCES_DIR)/main.cpp \
		  $(SOURCES_DIR)/Widget.cpp \
		  $(SOURCES_DIR)/WidgetText.cpp \

OBJS = $(SOURCES:.cpp=.o)

all: $(NAME)

%.o: %.cpp $(DEPENDENCIES)
	$(CC) -c $< -o $@ $(CFLAGS)

$(NAME): $(OBJS)
	@echo Creating $(NAME)...
	$(CC) -o $(NAME) $(OBJS)

clean:
	@rm -f $(OBJS)
	@echo Deleting $(NAME) OBJECTS files...

fclean: clean
	@rm -f $(NAME)
	@rm -rf $(NAME).dSYM
	@echo Deleting $(NAME)...

re: fclean all

.PHONY: clean fclean re all

