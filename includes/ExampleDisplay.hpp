#ifndef EXAMPLE_DISPLAY_HPP
# define EXAMPLE_DISPLAY_HPP

#include "IMonitorDisplay.hpp"
#include "WidgetText.hpp"

class ExampleDisplay : public IMonitorDisplay
{
private :
	IMonitorModule *		_modules[3];

// Init Function
	void					initModules(void);
	
public :
// Coplien
	ExampleDisplay();
	~ExampleDisplay();
	ExampleDisplay(ExampleDisplay const & ref);
	ExampleDisplay &		operator=(ExampleDisplay const & rhs);

// Interface
	IMonitorModule *&		operator[](int index);
	void					startMonitoring(int time);

// Interface Display
	void					displayWidget(Widget const & w) const;
	void					displayWidgetText(WidgetText const & w) const;
};

#endif /* !EXAMPLE_DISPLAY_HPP */
