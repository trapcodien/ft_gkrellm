#ifndef I_MONITOR_MODULE_HPP
# define I_MONITOR_MODULE_HPP

#include <iostream>
#include <vector>
#include "Widget.hpp"

class Widget;
typedef std::vector<Widget *>		Widgets;

class IMonitorModule
{
public :
	virtual std::string const &				getTitle(void) const = 0;
	virtual void							setTitle(std::string const & title) = 0;
	virtual Widgets const &					getWidgets(void) const = 0;
	virtual void							exec(void) = 0;
};

#endif /* !I_MONITOR_MODULE_HPP */
