#ifndef WIDGET_HPP
# define WIDGET_HPP

#include <iostream>
#include "IMonitorDisplay.hpp"

class IMonitorDisplay;

class Widget
{
private :
	std::string		_name;

public :
// Constructor
	Widget(std::string const & name);

// Coplien
	Widget();
	virtual ~Widget();
	Widget(Widget const & ref);
	Widget &				operator=(Widget const & rhs);

// Getters
	std::string const & 	getName(void) const;

// Display
	virtual void 			display(IMonitorDisplay const & displayer) const;
};

#endif /* !WIDGET_HPP */
