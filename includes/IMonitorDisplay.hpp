#ifndef I_MONITOR_DISPLAY_HPP
# define I_MONITOR_DISPLAY_HPP

# include <iostream>
# include "IMonitorModule.hpp"

class Widget;
class WidgetText;
class IMonitorModule;

class IMonitorDisplay
{
public :
	virtual IMonitorModule *&	operator[](int index) = 0;
	virtual void				startMonitoring(int time) = 0; // UI LOOP
	virtual void				displayWidget(Widget const & w) const = 0;
	virtual void				displayWidgetText(WidgetText const & w) const = 0;
};

#endif /* !I_MONITOR_DISPLAY_HPP */
