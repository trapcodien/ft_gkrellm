#ifndef WIDGET_TEXT_HPP
# define WIDGET_TEXT_HPP

# include <iostream>
# include "IMonitorDisplay.hpp"

class WidgetText : public Widget
{
private :
	std::string		_text;

public :
// Constructors
	WidgetText(std::string const & name);
	WidgetText(std::string const & name, std::string const & text);

// Coplien
	WidgetText();
	~WidgetText();
	WidgetText(WidgetText const & ref);
	WidgetText &		operator=(WidgetText const & rhs);

// Getters
	std::string const &	getText(void) const;

// Setters
	void				setText(std::string const & text);

// Display
	void 				display(IMonitorDisplay const & displayer) const;
};

#endif /* !WIDGET_TEXT_HPP */
