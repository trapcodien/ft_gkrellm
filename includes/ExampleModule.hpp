#ifndef EXAMPLE_MODULE_HPP
# define EXAMPLE_MODULE_HPP

#include "IMonitorModule.hpp"
#include "WidgetText.hpp"

class ExampleModule : public IMonitorModule
{
private :
	std::string			_title;
	Widgets				_widgets;
	int					_nbRefresh; // Just for this examplw

// Private
	static std::string	intToStr(int n);

public :
// Constructor
	ExampleModule(std::string const & title, std::string const & text);

// Coplien
	ExampleModule();
	~ExampleModule();
	ExampleModule(ExampleModule const & ref);
	ExampleModule &		operator=(ExampleModule const & rhs);

// Interface
	virtual std::string const &				getTitle(void) const;
	virtual void							setTitle(std::string const & title);
	virtual Widgets const &					getWidgets(void) const;
	virtual void							exec(void);

// Getter
	int										getNbRefresh(void) const;
};

#endif /* !EXAMPLE_MODULE_HPP */
